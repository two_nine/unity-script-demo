using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 资源文件
/// </summary>
[CreateAssetMenu(fileName = "New Data", menuName = "Character States/Data")]
public class CharacterData_SO : ScriptableObject
{
    [Header("States Info")]
    public int maxHealth;

    public int currentHealth;
    public int baseDefence;
    public int currentDefence;
}