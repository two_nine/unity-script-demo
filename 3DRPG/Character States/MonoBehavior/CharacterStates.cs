using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStates : MonoBehaviour
{
    public CharacterData_SO characterData;//角色自身数值
    public AttackData_SO attackData;//攻击相关数值

    [HideInInspector]//不在Inspector窗口显示
    public bool isCritical;//是否暴击

    #region Read From Data_SO

    public int MaxHealth
    {
        get
        {
            if (characterData != null)
            {
                return characterData.maxHealth;
            }
            else { return 0; }
        }
        set
        {
            if (characterData != null)
            {
                characterData.maxHealth = value;
            }
        }
    }

    public int CurrentHealth
    {
        get
        {
            if (characterData != null)
            {
                return characterData.currentHealth;
            }
            else
            {
                return 0;
            }
        }
        set
        {
            if (characterData != null)
            {
                characterData.currentHealth = value;
            }
        }
    }

    public int BaseDefence
    {
        get
        {
            if (characterData != null)
            {
                return characterData.baseDefence;
            }
            else
            {
                return 0;
            }
        }
        set
        {
            if (characterData != null)
            {
                characterData.baseDefence = value;
            }
        }
    }

    public int CurrentDefence
    {
        get
        {
            if (characterData != null)
            {
                return characterData.currentDefence;
            }
            else
            {
                return 0;
            }
        }
        set
        {
            if (characterData != null)
            {
                characterData.currentDefence = value;
            }
        }
    }

    #endregion Read From Data_SO

    #region Character Combat

    /// <summary>
    /// 造成伤害
    /// </summary>
    public void TakeDamage(CharacterStates attacker, CharacterStates defener)
    {
        int damage = Mathf.Max(attacker.CurrentDamage() - defener.CurrentDefence, 0);//要么是实际伤害要么为0

        CurrentHealth = Mathf.Max(CurrentHealth - damage, 0);//保证血量最少为0

        //TODO:Update UI
        //TODO:经验升级
        if (attacker.isCritical)//判断攻击者是否有暴击
        {
            defener.GetComponent<Animator>().SetTrigger("Hit");//当攻击者造成暴击时给被攻击者加上受击动画
        }
    }

    private int CurrentDamage()
    {
        float coreDamage = UnityEngine.Random.Range(attackData.minDamge, attackData.maxDamge);

        if (isCritical)
        {
            coreDamage *= attackData.criticalMultiplier;//暴击的伤害
            Debug.Log($"暴击！{coreDamage}");
        }

        return (int)coreDamage;
    }

    #endregion Character Combat
}