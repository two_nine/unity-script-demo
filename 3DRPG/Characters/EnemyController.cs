using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum EnemyStates
{
    GUARD, PATROL, CHASE, DEAD//守卫，巡逻，追击，死亡
}

[RequireComponent(typeof(NavMeshAgent))]//约束，拖拽脚本到物体上将会自动添加
public class EnemyController : MonoBehaviour
{
    private EnemyStates enemyStates;
    private NavMeshAgent agent;
    private GameObject attackTarget;//敌人的攻击目标
    public bool isGuard;//判断敌人类型
    private float speed;//存储原本的速度
    private Animator animator;//动画控制器
    private CharacterStates characterStates;//角色数值
    private float lastAttackTime;//上一次攻击时间
    private Collider coll;//碰撞体

    [Header("Basic Settings")]
    public float sightRadius;//可视范围

    private bool isWalk;
    private bool isChase;
    private bool isFollow;
    private bool isDead;

    public float lookAtTime;//巡逻到点之后等待时间
    private float remainLookAtTime;

    private Quaternion guardRotation;//记录角度

    [Header("Patrol State")]
    public float patrolRange;//巡逻范围

    private Vector3 wayPoint;//随机点
    private Vector3 guardPoint;//守卫点

    /// <summary>
    /// 私有变量的初始化
    /// </summary>
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        speed = agent.speed;
        characterStates = GetComponent<CharacterStates>();
        remainLookAtTime = lookAtTime;
        guardPoint = transform.position;
        guardRotation = transform.rotation;
        coll = GetComponent<Collider>();
    }

    /// <summary>
    /// 执行顺序排在Awake后面
    /// </summary>
    private void Start()
    {
        if (isGuard)
        {
            enemyStates = EnemyStates.GUARD;
        }
        else
        {
            enemyStates = EnemyStates.PATROL;
            GetNewWayPoint();
        }
    }

    private void Update()
    {
        if (characterStates.CurrentHealth == 0)
        {
            isDead = true;
        }
        SwitchStates();
        SwitchAnimation();
        lastAttackTime -= Time.deltaTime;
    }

    /// <summary>
    /// 关联动画bool值
    /// </summary>
    private void SwitchAnimation()
    {
        animator.SetBool("Walk", isWalk);
        animator.SetBool("Chase", isChase);
        animator.SetBool("Follow", isFollow);
        animator.SetBool("Critical", characterStates.isCritical);
        animator.SetBool("Death", isDead);
    }

    private void SwitchStates()
    {
        if (isDead)
        {
            enemyStates = EnemyStates.DEAD;
        }
        else
        if (FoundPlayer())
        {
            enemyStates = EnemyStates.CHASE;
        }
        switch (enemyStates)
        {
            case EnemyStates.GUARD:
                {
                    isChase = false;
                    if (transform.position != guardPoint)
                    {
                        isWalk = true;
                        agent.isStopped = false;
                        agent.destination = guardPoint;

                        //SqrMagnitude性能开销小于Distance
                        if (Vector3.SqrMagnitude(guardPoint - transform.position) <= agent.stoppingDistance)
                        {
                            isWalk = false;
                            transform.rotation = Quaternion.Lerp(transform.rotation, guardRotation, .01f);//缓慢转回角度
                        }
                    }
                }
                break;

            case EnemyStates.PATROL:
                {
                    isChase = false;
                    agent.speed = speed * .5f;
                    //判断是否到了随机巡逻点
                    if (Vector3.Distance(wayPoint, transform.position) <= agent.stoppingDistance)
                    {
                        isWalk = false;
                        if (remainLookAtTime > 0)
                        {
                            remainLookAtTime -= Time.deltaTime;
                        }
                        else
                            GetNewWayPoint();
                    }
                    else
                    {
                        isWalk = true;
                        agent.destination = wayPoint;
                    }
                }
                break;

            case EnemyStates.CHASE:
                {
                    isWalk = false;
                    isChase = true;
                    agent.speed = speed;
                    if (!FoundPlayer())
                    {
                        //Player跑出范围则回到之前状态；
                        isFollow = false;
                        if (remainLookAtTime > 0)
                        {
                            agent.destination = transform.position;
                            remainLookAtTime -= Time.deltaTime;
                        }
                        else if (isGuard)
                            enemyStates = EnemyStates.GUARD;
                        else
                            enemyStates = EnemyStates.PATROL;
                    }
                    else
                    {
                        agent.destination = attackTarget.transform.position;
                        isFollow = true;
                        agent.isStopped = false;//让角色不被束缚
                    }
                    if (TargetInAttackRange() || TargetInSkillRange())
                    {
                        isFollow = false;
                        agent.isStopped = true;
                        if (lastAttackTime < 0)
                        {
                            //还原冷却时间
                            lastAttackTime = characterStates.attackData.coolDown;

                            //暴击判断,若随机生成的值小于暴击值，则是暴击，否则为不暴击
                            characterStates.isCritical = Random.value < characterStates.attackData.criticalChance;

                            //执行攻击
                            Attack();
                        }
                    }
                }
                break;

            case EnemyStates.DEAD:
                {
                    coll.enabled = false;
                    agent.enabled = false;

                    Destroy(gameObject, 2f);
                }
                break;
        }
    }

    /// <summary>
    /// 执行攻击
    /// </summary>
    private void Attack()
    {
        transform.LookAt(attackTarget.transform);//面对攻击目标

        if (TargetInAttackRange())
        {
            //近程攻击动画
            animator.SetTrigger("Attack");
        }
        if (TargetInSkillRange())
        {
            //远程攻击动画
            animator.SetTrigger("Skill");
        }
    }

    /// <summary>
    /// 近战攻击范围
    /// </summary>
    /// <returns></returns>
    private bool TargetInAttackRange()
    {
        if (attackTarget != null)
        {
            return Vector3.Distance(
                attackTarget.transform.position, transform.position
                ) <= characterStates.attackData.attackRange;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 远程攻击范围
    /// </summary>
    /// <returns></returns>
    private bool TargetInSkillRange()
    {
        if (attackTarget != null)
        {
            return Vector3.Distance(
                attackTarget.transform.position, transform.position
                ) <= characterStates.attackData.skillRange;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 查找玩家
    /// </summary>
    /// <returns></returns>
    private bool FoundPlayer()
    {
        var colliders = Physics.OverlapSphere(transform.position, sightRadius);//在敌人碰撞体为中心点以半径来查找
        foreach (var target in colliders)//遍历在半径范围内找到的碰撞体
        {
            if (target.CompareTag("Player"))
            {
                //找到玩家
                attackTarget = target.gameObject;
                return true;
            }
        }
        attackTarget = null;
        return false;
    }

    /// <summary>
    /// 获取随机移动的坐标
    /// </summary>
    private void GetNewWayPoint()
    {
        remainLookAtTime = lookAtTime;
        float randomX = Random.Range(-patrolRange, patrolRange);//随机x坐标
        float randomZ = Random.Range(-patrolRange, patrolRange);//随机z坐标

        Vector3 randomPoint = new Vector3(
            guardPoint.x + randomX,
            transform.position.y,
            guardPoint.z + randomZ);

        //剔除无法到达的点导致的角色卡住问题
        NavMeshHit hit;
        //1为Navigation中Areas中的layer
        wayPoint = NavMesh.SamplePosition(randomPoint, out hit, patrolRange, 1) ? hit.position : transform.position;
    }

    /// <summary>
    /// 当选中当前目标的时候才会画出
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        //设置线条的颜色
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, sightRadius);//空心的
    }

    /// <summary>
    /// Animation Event
    /// </summary>
    /// <param name="attacker"></param>
    /// <param name="defencer"></param>
    private void Hit()
    {
        if (attackTarget != null)
        {
            var targetStates = attackTarget.GetComponent<CharacterStates>();

            targetStates.TakeDamage(characterStates, targetStates);
        }
    }
}