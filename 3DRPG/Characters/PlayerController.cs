using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;
    private Animator animator;
    private GameObject attackTarget;//攻击目标
    private float lastAttackTime;//上次攻击时间
    private CharacterStates characterStates;//存储角色数值

    private bool isDead;

    /// <summary>
    /// 游戏执行最优先获得引用，自身变量获取一般都在awake中。
    /// </summary>
    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        characterStates = GetComponent<CharacterStates>();
    }

    private void Start()
    {
        //事件的添加，注册/订阅的方式+=
        MouseManager.Instance.OnMouseClicked += MoveToTarget;
        MouseManager.Instance.OnEnemyClicked += EventAttack;

        GameManager.Instance.RigisterPlayer(characterStates);
    }

    private void Update()
    {
        isDead = characterStates.CurrentHealth == 0;
        SwitchAnimation();
        lastAttackTime -= Time.deltaTime;//冷却衰减
    }

    /// <summary>
    /// 根据状态切换动画
    /// </summary>
    private void SwitchAnimation()
    {
        animator.SetFloat("Speed", agent.velocity.sqrMagnitude);//sqrMagnitude将Vector3的坐标值转换为浮点数
        animator.SetBool("Death", isDead);
    }

    /// <summary>
    /// 移动到位置
    /// </summary>
    /// <param name="target"></param>
    public void MoveToTarget(Vector3 target)
    {
        //停止所有的协程，打断攻击来进行移动
        StopAllCoroutines();
        agent.isStopped = false;//还原状态
        agent.destination = target;
    }

    /// <summary>
    /// 移动到敌人位置并攻击
    /// </summary>
    /// <param name="target"></param>
    private void EventAttack(GameObject target)
    {
        if (target != null)
        {
            attackTarget = target;
            characterStates.isCritical = UnityEngine.Random.value < characterStates.attackData.criticalChance;
            //启动协程
            StartCoroutine(MoveToAttackTarget());
        }
    }

    /// <summary>
    /// 协程
    /// </summary>
    /// <returns></returns>
    private IEnumerator MoveToAttackTarget()
    {
        agent.isStopped = false;//没有停止
        transform.LookAt(attackTarget.transform);//转向攻击目标

        //Distance计算两个物体之间的距离，Vector3则是3D
        while (
            Vector3.Distance(attackTarget.transform.position, transform.position)
            > characterStates.attackData.attackRange)
        {
            agent.destination = attackTarget.transform.position;
            //暂停协程的执行，等待下一帧继续执行
            yield return null;
        }

        agent.isStopped = true;//真的停了
        //跳出循环之后则是攻击
        if (lastAttackTime < 0)
        {
            //是否暴击
            animator.SetBool("Critical", characterStates.isCritical);
            animator.SetTrigger("Attack");
            //重置冷却时间
            lastAttackTime = characterStates.attackData.coolDown;
        }
    }

    /// <summary>
    /// Animation Event
    /// </summary>
    private void Hit()
    {
        //获取到被攻击者的状态
        var targetStates = attackTarget.GetComponent<CharacterStates>();

        targetStates.TakeDamage(characterStates, targetStates);
    }
}