using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 泛型单例,where约束T的类型
/// </summary>
/// <typeparam name="T"></typeparam>
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T instance;//单例

    public static T Instance
    {
        get
        {
            return instance;
        }
    }

    /// <summary>
    /// 可被子类重写Awake
    /// </summary>
    protected virtual void Awake()
    {
        if (instance != null)
        {
            Destroy(instance);
        }
        else
        {
            instance = (T)this;
        }
    }

    /// <summary>
    /// 判断当前单例类型是否已存在
    /// </summary>
    public static bool IsInitialized
    {
        get { return instance != null; }
    }

    /// <summary>
    /// 当被销毁时清空instance
    /// </summary>
    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
}