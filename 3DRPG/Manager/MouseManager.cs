using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// 鼠标事件
/// </summary>
//[Serializable]//序列化
//public class EventVector3 : UnityEvent<Vector3>
//{ }

public class MouseManager : Singleton<MouseManager>
{
    private RaycastHit hitInfo;//射线碰撞到的物体的相关信息

    //public EventVector3 OnMouseClicked;
    public event Action<Vector3> OnMouseClicked;//点击地面进行行走

    public event Action<GameObject> OnEnemyClicked;//点击敌人行走到敌人面前进行攻击

    //2D图片变量
    public Texture2D point, doorway, attack, target, arrow;

    protected override void Awake()
    {
        base.Awake();
    }

    private void Update()
    {
        SetCursorTexture();
        MouseControl();
    }

    /// <summary>
    /// 设置指针的贴图
    /// </summary>
    private void SetCursorTexture()
    {
        //从主摄像机发出，到鼠标点击的点的射线
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hitInfo))
        {
            //切换鼠标的贴图

            switch (hitInfo.collider.gameObject.tag)
            {
                case "Ground":
                    {
                        Cursor.SetCursor(target, new Vector2(16, 16), CursorMode.Auto);
                    }
                    break;

                case "Enemy":
                    {
                        Cursor.SetCursor(attack, new Vector2(16, 16), CursorMode.Auto);
                    }
                    break;
            }
        }
    }

    private void MouseControl()
    {
        //检测鼠标按下事件，且射线的碰撞体信息不为空（没有点击到地图外面）
        if (Input.GetMouseButtonDown(0) && hitInfo.collider != null)
        {
            //检测射线返回物体碰撞体的标签为地面
            if (hitInfo.collider.gameObject.CompareTag("Ground"))
            {
                OnMouseClicked?.Invoke(hitInfo.point);
            }
            if (hitInfo.collider.gameObject.CompareTag("Enemy"))
            {
                OnEnemyClicked?.Invoke(hitInfo.collider.gameObject);
            }
        }
    }
}