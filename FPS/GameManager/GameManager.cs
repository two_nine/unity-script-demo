using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //单例模式
    public static GameManager Singleton;//通过类名调用方法需要给方法加上static，若是通过单例模式调用方法则是调用的实例，不需要加上static

    //字典存储角色名字
    private Dictionary<string, Player> players = new Dictionary<string, Player>();

    [SerializeField]
    public MatchSetting MatchSetting;//存储全局游戏设置

    //private static string info;

    //public static void UpdateInfo(string _info)
    //{
    //    info = _info;
    //}

    private void Awake() //初始化变量一般都在awake中执行，早于start函数
    {
        Singleton = this;
        Screen.SetResolution(1080, 920, false);//限制窗口大小，全屏模式为false
    }

    public void RegisterPlayer(string name, Player player)//将玩家加入玩家集合
    {
        player.transform.name = name;
        players.Add(name, player);
    }

    public void UnRegisterPlayer(string name)//从玩家集合中删除
    {
        players.Remove(name);
    }

    public Player GetPlayer(string name)//通过名称来获取一名玩家
    {
        return players[name];
    }

    //在页面上画画,每一帧至少调用一次
    private void OnGUI()
    {
        //开启一片区域，在区域中作画
        GUILayout.BeginArea(new Rect(200f, 200f, 200f, 400f));
        //垂直展示
        GUILayout.BeginVertical();

        GUI.color = Color.black;
        foreach (string name in players.Keys)
        {
            Player player = GetPlayer(name);
            GUILayout.Label(name + " - " + player.GetHealth());
        }

        //有开启就有结束
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}