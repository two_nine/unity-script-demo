using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class NetWorkManagerUI : MonoBehaviour
{
    //取出场景中的元素
    [SerializeField]
    private Button HostBtn;

    [SerializeField]
    private Button ServerBtn;

    [SerializeField]
    private Button ClientBtn;

    // Start is called before the first frame update
    private void Start()
    {
        HostBtn.onClick.AddListener(() =>
        {
            //单例模式singleton
            NetworkManager.Singleton.StartHost();
            DestroyAllButtons();
        });

        ServerBtn.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartServer();
            DestroyAllButtons();
        });

        ClientBtn.onClick.AddListener(() =>
        {
            NetworkManager.Singleton.StartClient();
            DestroyAllButtons();
        });
    }

    private void DestroyAllButtons()
    {
        Destroy(HostBtn.gameObject);
        Destroy(ServerBtn.gameObject);
        Destroy(ClientBtn.gameObject);
    }
}