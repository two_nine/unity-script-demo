using Unity.Netcode.Components;
using UnityEngine;

namespace Unity.Multiplayer.Samples.Utilities.ClientAuthority
{
    [DisallowMultipleComponent]
    public class ClientNetworkTransform : NetworkTransform
    {
        /// <summary>
        /// 是否只能在服务器端授权，关闭之后客户端就可以使用
        /// </summary>
        /// <returns></returns>
        protected override bool OnIsServerAuthoritative()
        {
            return false;
        }
    }
}