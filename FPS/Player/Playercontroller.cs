﻿using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Playercontroller : NetworkBehaviour
{
    //获取物体的物理属性
    [SerializeField]
    private Rigidbody rb;

    //因为上下旋转的是摄像机所以需要获取到相机的对象
    [SerializeField]
    private Camera cam;

    //速度,每秒钟移动的距离
    private Vector3 velocity = Vector3.zero;//速度

    //设置最大旋转角度
    [SerializeField]
    private float cameraRotationLimit = 85f;

    private float cameraRotationTotal = 0f;//累计旋转多少度

    private Vector3 yRotation = Vector3.zero; //旋转角色实现转身的效果
    private Vector3 xRotation = Vector3.zero; //旋转摄像机实现抬头低头的功能
    private Vector3 thrusterForce = Vector3.zero;//向上的推力，默认也是0
    private float recoilForce = 0f;//后坐力

    private Vector3 lastFramePosition = Vector3.zero;//上一帧坐标
    private float eps = 0.01f;//误差值
    private Animator animator;//操作动画
    private float distToGround = 0f;//从角色模型中心向下发射射线的长度

    private void Start()
    {
        lastFramePosition = transform.position;
        animator = GetComponentInChildren<Animator>();
        distToGround = GetComponent<Collider>().bounds.extents.y;//碰撞检测范围api
    }

    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }

    //旋转函数
    public void Rotation(Vector3 _yRotaion, Vector3 _xRotation)
    {
        yRotation = _yRotaion;
        xRotation = _xRotation;
    }

    public void Thrust(Vector3 _thrusterForce)
    {
        thrusterForce = _thrusterForce;
    }

    private void PerformMovement()
    {
        //当速度不等于0的时候再进行后续操作,减少许多资源的浪费
        if (velocity != Vector3.zero)
        {
            //将物体移动到什么位置上,上一秒所处位置+速度*时间间隔(库函数)
            rb.MovePosition(rb.position + velocity * Time.fixedDeltaTime);
        }
        if (thrusterForce != Vector3.zero)
        {
            //给刚体作用一个力，作用0.02秒 (Time.fixedDeltaTime)
            rb.AddForce(thrusterForce, ForceMode.Force);

            thrusterForce = Vector3.zero;
        }
    }

    public void AddRecoilForce(float newRecoilFoece)
    {
        recoilForce += newRecoilFoece;
    }

    private void PerformRotation()
    {
        if (recoilForce < 0.1)
        {
            recoilForce = 0;
        }

        if (xRotation != Vector3.zero || recoilForce > 0)
        {
            //累计一共转了多少度
            cameraRotationTotal += xRotation.x - recoilForce;
            //值小于-85则改为-85，大于85则改为85,夹逼
            cameraRotationTotal = Mathf.Clamp(cameraRotationTotal, -cameraRotationLimit, cameraRotationLimit);
            cam.transform.localEulerAngles = new Vector3(cameraRotationTotal, 0f, 0f);
        }
        if (yRotation != Vector3.zero || recoilForce > 0)
        {
            rb.transform.Rotate(yRotation + rb.transform.up * Random.Range(-2f * recoilForce, 2f * recoilForce));
        }

        recoilForce *= 0.5f;
    }

    private void PerformAnimation()
    {
        Vector3 deltaPosition = transform.position - lastFramePosition;
        lastFramePosition = transform.position;

        //点积法判断
        float forward = Vector3.Dot(deltaPosition, transform.forward);
        float right = Vector3.Dot(deltaPosition, transform.right);

        int direction = 0;
        if (forward > eps)//大于误差则意味着改变状态
        {
            direction = 1;//前
        }
        else if (forward < -eps)//后
        {
            if (right > eps)
            {
                direction = 4;//右后
            }
            else if (right < -eps)
            {
                direction = 6;//左后
            }
            else
            {
                direction = 5;//正后
            }
        }
        else if (right > eps)
        {
            direction = 3;//右
        }
        else if (right < -eps)
        {
            direction = 7;//左
        }
        if (GetComponent<Player>().IsDead())
        {
            direction = -1;
        }
        if (!Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f))//发射射线进行碰撞检测
        {
            direction = 8;
        }
        animator.SetInteger("direction", direction);
    }

    //如果要模拟物理过程则不能用update需要使用FixedUpdate
    private void FixedUpdate()//执行时间间隔很严格,默认设置是严格0.02秒执行一次
    {
        if (IsLocalPlayer)
        {
            PerformMovement();
            PerformRotation();
            PerformAnimation();
        }
    }

    private void Update()
    {
        if (!IsLocalPlayer)
        {
            PerformAnimation();
        }
    }
}