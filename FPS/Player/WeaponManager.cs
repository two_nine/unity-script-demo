using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class WeaponManager : NetworkBehaviour
{
    [SerializeField]
    private PlayerWeapon[] weapons;

    [SerializeField]
    private GameObject weaponHolder;

    //[SerializeField]
    //private PlayerWeapon secondaryWeapon;

    //[SerializeField]
    //private PlayerWeapon thirdWeapon;

    private PlayerWeapon currentWeapon;

    /// <summary>
    /// 当前武器的特效
    /// </summary>
    private WeaponGraphics currentGraphics;

    /// <summary>
    /// 当前音效
    /// </summary>
    private AudioSource currentAudioSource;

    // Start is called before the first frame update
    private void Start()
    {
        EquipWeapon(weapons[0]);
    }

    public PlayerWeapon CurWeap
    { get { return currentWeapon; } }

    /// <summary>
    /// 装备武器
    /// </summary>
    /// <param name="weapon"></param>
    public void EquipWeapon(PlayerWeapon weapon)
    {
        currentWeapon = weapon;

        while (weaponHolder.transform.childCount > 0)
        {
            //Destroy是异步销毁，DestroyImmediate是同步销毁,暂时不能使用Destroy，因为会卡死unity
            DestroyImmediate(weaponHolder.transform.GetChild(0).gameObject);//需要删物体的对象不能直接删物体
        }

        GameObject weaponObject = Instantiate(currentWeapon.graphics, weaponHolder.transform.position, weaponHolder.transform.rotation);//Instantiate实例化对象，传入对象和初始化坐标

        weaponObject.transform.SetParent(weaponHolder.transform);

        //在装备武器的时候取出武器的特效
        currentGraphics = weaponObject.GetComponent<WeaponGraphics>();
        //设置音效
        currentAudioSource = weaponObject.GetComponent<AudioSource>();
        //判断是否是自己的枪声，若是则将自己枪声变为2d，避免产生空间感
        if (IsLocalPlayer)
        {
            currentAudioSource.spatialBlend = 0f;
        }
    }

    //只是在本地进行切换的操作，但是其他客户端看不见
    private void ToggleWeapon(float direction)
    {
        var weaponNum = Array.IndexOf(weapons, currentWeapon);
        if (direction > 0)
        {
            --weaponNum;
            EquipWeapon(weapons[weaponNum < 0 ? weapons.Length - 1 : weaponNum]);
            Debug.Log(currentWeapon.gunName);
        }
        else if (direction < 0)
        {
            ++weaponNum;
            EquipWeapon(weapons[weaponNum >= weapons.Length ? 0 : weaponNum]);
            Debug.Log(currentWeapon.gunName);
        }
    }

    /// <summary>
    /// 返回当前的特效
    /// </summary>
    /// <returns></returns>
    public WeaponGraphics GetCurrentGraphics()
    {
        return currentGraphics;
    }

    /// <summary>
    /// 返回当前的音效
    /// </summary>
    /// <returns></returns>
    public AudioSource GetCurrentAudioSource()
    {
        return currentAudioSource;
    }

    [ClientRpc]
    private void ToggleWeaponClientRpc(float direction)
    {
        ToggleWeapon(direction);
    }

    [ServerRpc]
    private void ToggleWeaponServerRpc(float direction)//将切换的操作同步给服务端
    {
        if (!IsHost)
            ToggleWeapon(direction);//服务端自己做完切换操作
        ToggleWeaponClientRpc(direction);//再调用客户端同步函数，将切换的操作同步给客户端
    }

    // Update is called once per frame
    private void Update()
    {
        if (IsLocalPlayer)
        {
            var direction = Input.GetAxis("Mouse ScrollWheel");
            if (direction != 0)
            {
                ToggleWeaponServerRpc(direction);
            }
        }
    }
}