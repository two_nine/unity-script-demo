using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerSetUp : NetworkBehaviour
{
    [SerializeField]
    private Behaviour[] ComponetsToDisable;

    private Camera SceneCamera;

    // Start is called before the first frame update
    public override void OnNetworkSpawn()//当游戏对象成功链接到网络的时候会执行
    {
        base.OnNetworkSpawn();
        if (!IsLocalPlayer)
        {
            //for (int i = 0; i < ComponetsToDisable.Length; i++)
            //{
            //    ComponetsToDisable[i].enabled = false;
            //}
            SetLayerMaskForAllChildren(transform, LayerMask.NameToLayer("Remote Player"));
            DisableComponents();
        }
        else
        {
            SetLayerMaskForAllChildren(transform, LayerMask.NameToLayer("Player"));
            //有玩家则禁用主摄像头
            SceneCamera = Camera.main;
            if (SceneCamera != null)
            {
                SceneCamera.gameObject.SetActive(false);
            }
        }
        string name = "Player" + GetComponent<NetworkObject>().NetworkObjectId.ToString();
        Player player = GetComponent<Player>();
        player.Setup();//需要等前面赋值完才能进行存储
        GameManager.Singleton.RegisterPlayer(name, player);
    }

    private void SetLayerMaskForAllChildren(Transform transform, LayerMask layerMask)
    {
        transform.gameObject.layer = layerMask;
        for (int i = 0; i < transform.childCount; i++)
        {
            SetLayerMaskForAllChildren(transform.GetChild(i), layerMask);
        }
    }

    private void DisableComponents()
    {
        for (int i = 0; i < ComponetsToDisable.Length; i++)
        {
            ComponetsToDisable[i].enabled = false;
        }
    }

    public override void OnNetworkDespawn()//当一个客户端断开链接之前会调用
    {
        base.OnNetworkDespawn();
        if (SceneCamera != null)
        {
            SceneCamera.gameObject.SetActive(true);
        }

        GameManager.Singleton.UnRegisterPlayer(transform.name);
    }
}