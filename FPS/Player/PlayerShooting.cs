using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class PlayerShooting : NetworkBehaviour
{
    private const string PLAYER_TAG = "Player";

    //[SerializeField]
    //private PlayerWeapon weapon;

    private WeaponManager weaponManager;

    //记录当前武器
    private PlayerWeapon currentWeapon;

    private Playercontroller playerController;

    private float shootCoolDownTime = 0f;//冷却时间

    private int autoShootCount = 0;//当前一共连开多少枪

    private Camera cam;

    private enum HitEffectMaterial//击中效果的材质
    {
        Metal,
        Stone,
    }

    [SerializeField]
    private LayerMask mask;//设置层，以达到不误伤队友的效果

    private void Start()
    {
        cam = GetComponentInChildren<Camera>();
        weaponManager = GetComponent<WeaponManager>();//动态获取引用
        playerController = GetComponent<Playercontroller>();// 获取引用
    }

    // Update is called once per frame
    private void Update()
    {
        shootCoolDownTime += Time.deltaTime;

        if (!IsLocalPlayer) return;//不是本地玩家则直接返回
        currentWeapon = weaponManager.CurWeap;

        if (currentWeapon.shootRate <= 0)//单发
        {
            //在project setting中的input manager查看按键映射
            if (Input.GetButtonDown("Fire1") && shootCoolDownTime >= currentWeapon.shootCoolDownTime)
            //识别两帧之间是否有键按下，单发,单发加上冷却时间
            {
                autoShootCount = 0;
                Shoot();
                shootCoolDownTime = 0f;
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))//按下鼠标
            {
                autoShootCount = 0;
                //Invoke触发，InvokeRepeating周期性触发
                InvokeRepeating("Shoot", 0f, 1f / currentWeapon.shootRate);//调用的函数名字，几秒后开始触发，每次触发的时间间隔
            }
            else if (Input.GetButtonUp("Fire1") || Input.GetAxis("Mouse ScrollWheel") != 0)//抬起鼠标
            {
                CancelInvoke("Shoot");//当按键被抬起来的时候结束触发函数
            }
        }
    }

    /// <summary>
    /// 每次射击相关的逻辑，包括特效，声音等
    /// </summary>
    private void OnShoot(float recoilForce)
    {
        weaponManager.GetCurrentGraphics().muzzleFlash.Play();
        weaponManager.GetCurrentAudioSource().Play();

        if (IsLocalPlayer)//如果是本地玩家才需要添加后坐力
        {
            playerController.AddRecoilForce(recoilForce);
        }
    }

    [ClientRpc]
    private void OnShootClientRpc(float recoilForce)//服务端给客户端发送
    {
        OnShoot(recoilForce);
    }

    [ServerRpc]
    private void OnShootServerRpc(float recoilForce)//客户端向服务端发请求
    {
        if (IsHost)//若是host端则server和client在一起
            OnShoot(recoilForce);//先在服务器端渲染
        OnShootClientRpc(recoilForce);//服务器端渲染完成之后，再调用客户端的渲染
    }

    /// <summary>
    /// 渲染击中效果
    /// </summary>
    /// <param name="pos"></param>
    /// <param name="normal"></param>
    /// <param name="material"></param>
    private void OnHit(Vector3 pos, Vector3 normal, HitEffectMaterial material)//方向，法向，类别
    {
        //获取到该渲染哪种效果对象
        GameObject hitEffectPrefab;
        if (material == HitEffectMaterial.Metal)
        {
            hitEffectPrefab = weaponManager.GetCurrentGraphics().metalHitEffectPrefab;
        }
        else
        {
            hitEffectPrefab = weaponManager.GetCurrentGraphics().stoneHitEffectPrefab;
        }
        //渲染/生成效果, Quaternion.LookRotation摄像机看去的方向
        GameObject hitEffectrObj = Instantiate(hitEffectPrefab, pos, Quaternion.LookRotation(normal));

        ParticleSystem particleSystem = hitEffectrObj.GetComponent<ParticleSystem>();
        particleSystem.Emit(1);//直接触发，避免延迟
        particleSystem.Play();
        Destroy(hitEffectrObj, 1f);//1秒后删除该对象
    }

    [ServerRpc]
    private void OnHitServerRpc(Vector3 pos, Vector3 normal, HitEffectMaterial material)
    {
        if (IsHost)
            OnHit(pos, normal, material);
        OnHitClientRpc(pos, normal, material);
    }

    [ClientRpc]
    private void OnHitClientRpc(Vector3 pos, Vector3 normal, HitEffectMaterial material)
    {
        OnHit(pos, normal, material);
    }

    //射击
    private void Shoot()
    {
        autoShootCount++;

        float recoilForce = currentWeapon.recoilForce;
        if (autoShootCount < 3)
        {
            recoilForce *= 0.2f;
        }

        OnShootServerRpc(recoilForce);

        RaycastHit hit;//碰撞检测用
        //起点，方向，末尾物体
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask))//out将值传回来，类似于c++的引用
        {
            if (hit.collider.tag == PLAYER_TAG)
            {
                ShootServerRpc(hit.collider.name, currentWeapon.damage);
                OnHitServerRpc(hit.point, hit.normal, HitEffectMaterial.Metal);//渲染击中效果
            }
            else
            {
                OnHitServerRpc(hit.point, hit.normal, HitEffectMaterial.Stone);//渲染击中效果
            }
        }
    }

    //客户端可以调用服务端的函数
    [ServerRpc]//若是想让该注解起作用，需要将继承换位NetworkBehaviour,且函数名称必须以serverRpc结尾
    private void ShootServerRpc(string name, int damage)
    {
        Player player = GameManager.Singleton.GetPlayer(name);
        player.TaskDamage(damage);
    }
}