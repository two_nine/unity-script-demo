using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 能够串行化    武器类
/// </summary>
[Serializable]
public class PlayerWeapon
{
    public string gunName = "shotGun";//武器名称

    public int damage = 10;//伤害值

    public float range = 100f;//攻击距离

    public float shootRate = 0.5f; //一秒射击的子弹频率

    public float recoilForce = 2f;//后坐力

    public float shootCoolDownTime = 0.75f; //冷却时间
    public GameObject graphics;//渲染模型到界面上
}