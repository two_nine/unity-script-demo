using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponGraphics : MonoBehaviour
{
    public ParticleSystem muzzleFlash;//粒子效果，枪口火花

    public GameObject metalHitEffectPrefab;//击中金属的效果

    public GameObject stoneHitEffectPrefab;//击中石头的特效
}