using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Player : NetworkBehaviour//需要继承联网的类
{
    [SerializeField]
    private int maxHealth = 100;

    [SerializeField]
    private Behaviour[] componentsToDisable;//当玩家死亡之后需要禁用的组件数组,(控制)

    private bool[] componentsEnabled;//记录组件的状态

    private bool colliderEnabled;//碰撞的状态

    private NetworkVariable<bool> isDied = new NetworkVariable<bool>();//判断玩家是否死亡的标志位

    /// <summary>
    /// networkvariable，unity自动同步,只能在服务器端修改
    /// </summary>
    private NetworkVariable<int> currentHealth = new NetworkVariable<int>();

    public void Setup()
    {
        componentsEnabled = new bool[componentsToDisable.Length];
        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsEnabled[i] = componentsToDisable[i].enabled;//先存储所有组件的初始状态，方便后面玩家复活后进行启用
        }
        Collider col = GetComponent<Collider>();
        colliderEnabled = col.enabled;//得到碰撞的状态
        SetDefaults();
    }

    public bool IsDead()
    {
        return isDied.Value;
    }

    public void SetDefaults()
    {
        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsToDisable[i].enabled = componentsEnabled[i];//还原组件的值
        }
        Collider col = GetComponent<Collider>();
        col.enabled = colliderEnabled;//还原碰撞的状态

        if (IsServer)
        {
            currentHealth.Value = maxHealth;
            isDied.Value = false;
        }
    }

    private void DieOnServer()
    {
        Die();
    }

    [ClientRpc]//在客户端执行，函数名称需要以ClientRpc结尾,只能在服务器端调用
    private void DieClientRpc()
    {
        Die();
    }

    /// <summary>
    /// 死亡
    /// </summary>
    private void Die()
    {
        GetComponentInChildren<Animator>().SetInteger("direction", -1);
        GetComponent<Rigidbody>().useGravity = false;

        for (int i = 0; i < componentsToDisable.Length; i++)
        {
            componentsToDisable[i].enabled = false;//禁用
        }
        Collider col = GetComponent<Collider>();
        col.enabled = false;//禁用碰撞

        //死亡后使用startCoroutine调用一个新的线程去调用重生的函数
        StartCoroutine(Respawn());
    }

    /// <summary>
    /// IEnumerator表示可枚举的集合,重生
    /// </summary>
    /// <returns></returns>
    private IEnumerator Respawn()
    {
        //异步操作
        //当IEnumerable遇到yield 则会进行等待,yield会逐步生成值，而不是一次性生成，yield return是逐步生成并返回
        yield return new WaitForSeconds(GameManager.Singleton.MatchSetting.respawnTime);//WaitForSeconds等待三秒
        SetDefaults();
        GetComponentInChildren<Animator>().SetInteger("direction", 0);
        GetComponent<Rigidbody>().useGravity = true;
        if (IsLocalPlayer)
            transform.position = new Vector3(0, 10f, 0);
    }

    public void TaskDamage(int damage)//只在服务器端被调用
    {
        if (isDied.Value) return;
        currentHealth.Value -= damage;
        if (currentHealth.Value <= 0)
        {
            currentHealth.Value = 0;
            isDied.Value = true;
            if (!IsHost)
                DieOnServer();//若想在服务器执行的话需要手动调用
            DieClientRpc();//会在服务器端调用，然后在客户端执行
        }
    }

    public int GetHealth()
    {
        return currentHealth.Value;
    }
}