﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    //此注解是让此变量可以在unity中调试,且此值以unity中设置的为准
    [SerializeField]
    private float Speed = 5f;

    [SerializeField]
    private float dpi = 10000;

    //推力
    [SerializeField]
    private float thrusterForce = 20f;

    [SerializeField]
    private Playercontroller playercontroller;

    //[SerializeField]//获取弹簧组件（一种方法）
    //private ConfigurableJoint joint;

    private float distToGround = 0f;//从角色模型中心向下发射射线的长度

    // Start is called before the first frame update
    private void Start()
    {
        //初始化
        //游戏运行时锁鼠标
        Cursor.lockState = CursorLockMode.Locked;
        //开始的时候会进行查找，若是没找到则会报错（第二种方法获取组件）,若有多个则随机取一个
        //joint = GetComponent<ConfigurableJoint>();

        distToGround = GetComponent<Collider>().bounds.extents.y;//碰撞检测范围api
    }

    // Update is called once per frame
    private void Update()//执行的时间间隔不均匀
    {
        //获取用户输入横向移动
        float xMov = Input.GetAxisRaw("Horizontal");
        //获取用户输入纵向移动
        float yMov = Input.GetAxisRaw("Vertical");
        //三维向量,移动,transform调用的的是加入了本脚本的对象的Transform坐标,right则是红色的坐标,forward是蓝色的坐标
        //right初始表示向右的正方向,forward初始表示向前的正方向
        /*
            两个方向的向量和的模长不是1,为了物体向各个方向的速度都是一样的则需要使用normalized进行标准化
         */
        Vector3 velocity = (transform.right * xMov + transform.forward * yMov).normalized * Speed;
        playercontroller.Move(velocity);

        //获取鼠标的移动坐标
        float xMouse = Input.GetAxisRaw("Mouse X");
        float yMouse = Input.GetAxisRaw("Mouse Y");
        //print(xMouse.ToString() + " " + yMouse.ToString());
        //鼠标的横向移动对应的是坐标轴中y轴的转动
        Vector3 yRotation = new Vector3(0f, xMouse, 0f) * dpi;
        //鼠标竖直方向对应的是坐标轴中的x轴
        Vector3 xRotation = new Vector3(-yMouse, 0f, 0f) * dpi;

        playercontroller.Rotation(yRotation, xRotation);

        //Vector3 force = Vector3.zero;
        //按住空格键是跳，可以看unity中的按键映射
        if (Input.GetButton("Jump"))
        {
            if (Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f))//发射射线进行碰撞检测
            {
                //施加一个向上的力，Vector3.up坐标是0，1，0
                Vector3 force = Vector3.up * thrusterForce;

                //跳起来之后清空弹力,若是单个变量赋值有问题则将整个对象重新赋值
                //joint.yDrive = new JointDrive
                //{
                //    positionSpring = 0f,
                //    positionDamper = 0f,
                //    maximumForce = 0f,
                //};
                playercontroller.Thrust(force);
            }
        }
        //else
        //{
        //    //joint.yDrive = new JointDrive
        //    //{
        //    //    positionSpring = 20f,
        //    //    positionDamper = 0f,
        //    //    maximumForce = 40f,
        //    //};
        //}
        //playercontroller.Thrust(force);
    }
}